# C++ RogueLike game

A Roguelike game implemented in C++ following [http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_1:_setting_up](this) tutorial,
with a few modifications.

to compile run ``./compile.sh``, then run ``./play`` to... play.

## Controls

|    Key(s)     |          action           |
| ------------- | ------------------------- |
| ``esc``       | open pause menu           |
|``arrow keys`` | move the player           |
|``g``          | pick up item              |
|``i``          | open inventory (use)      |
|``d``          | open inventory (drop)     |
|``a...z``      | select item from inventory|
|``left click`` | target spell\action       |
|``right click``| abort spell\action        |
|``F11``        | toggle full screen        |

## Known bugs

* Pressing anything besides `a` to `z` in the inventory
  makes the game crash.
* Lightning bolt displays exponential value on damage (at least on trolls)

## Coming next

* more monsters
* better monster's logic and tracking
* more items
* monsters' progression (the deeper the stronger)
* randomized values for stats and attacks (maybe critical hits)