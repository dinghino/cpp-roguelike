#include "main.hpp"
#include <math.h>

// file-read-only constants
static const int ROOM_MAX_SIZE = 12;
static const int ROOM_MIN_SIZE = 6;
static const int MAX_ROOM_MONSTERS = 3;
static const int MAX_ROOM_ITEMS = 2;

static const int ORC_OR_TROLL_PERC = 80;
// how many turns a scent lasts
static const int SCENT_THRESHOLD = 20;

class BspListener : public ITCODBspCallback {
private:
    Map &map;           // map to dig
    int roomNum;        // room's number
    int lastX, lastY;   // center of last room

public:
    BspListener(Map &map) : map(map), roomNum(0) {}
    
    bool visitNode(TCODBsp *node, void *userData) {
        if (node->isLeaf()) {
            int x, y, w, h;
            bool withActors = (bool)userData;
            w = map.rng->getInt(ROOM_MIN_SIZE, node->w - 2);
            h = map.rng->getInt(ROOM_MIN_SIZE, node->h - 2);
            x = map.rng->getInt(node->x + 1, node->x + node->w -w-1);
            y = map.rng->getInt(node->y + 1, node->y + node->h -h-1);

            map.createRoom(roomNum == 0, x, y, x+ w - 1, y + h - 1, withActors);

            if (roomNum != 0) {
                // dig a corridor from last room
                map.dig(lastX, lastY, x + w / 2, lastY);
                map.dig(x + w / 2, lastY, x + w / 2, y + h / 2);
            }
            lastX = x + w / 2;
            lastY = y + h / 2;
            roomNum++;
        }
        return true;
    }
};

Map::Map(int width, int height) : width(width), height(height), currentScentValue(SCENT_THRESHOLD), scentThreshold(SCENT_THRESHOLD)
{
    seed = TCODRandom::getInstance()->getInt(0, 0x7FFFFFFF);
}

Map::~Map()
{
    delete [] tiles;
    delete map;
}

void Map::init(bool withActors)
{
    rng = new TCODRandom(seed, TCOD_RNG_CMWC);

    tiles = new Tile[width * height];
    map = new TCODMap(width, height);

    TCODBsp bsp(0, 0, width, height);
    bsp.splitRecursive(rng, 8, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
    BspListener listener(*this);
    bsp.traverseInvertedLevelOrder(&listener, (void *)withActors);
}

bool Map::isWall(int x, int y) const
{
    return !map->isWalkable(x, y);
}
bool Map::isExplored(int x, int y) const
{
    return tiles[x + y*width].explored;
}
bool Map::canWalk(int x, int y) const
{
    if (isWall(x, y)) return false;

    for (Actor **pActor=engine.actors.begin(); pActor!=engine.actors.end(); pActor++) {
        Actor *actor = *pActor;
        if (actor->blocks && actor->x == x && actor->y == y) {
            // something else is blocking here, cannot walk.
            return false;
        }
    }
    return true;
}
bool Map::isInFov(int x, int y) const
{
    if (x < 0 || x >= width || y < 0 || y >= height) return false;

    if (map->isInFov(x, y)) {
        tiles[x + y*width].explored = true;
        return true;
    }

    return false;
}

void Map::addMonster(int x, int y)
{
    TCODRandom *rng = TCODRandom::getInstance();
    if (rng->getInt(0, 100) < ORC_OR_TROLL_PERC) {
        // create an orc
        Actor *orc = new Actor(x, y, 'o', "orc", TCODColor::desaturatedGreen);
        orc->destructible = new MonsterDestructible(10, 0, "dead orc", 35);
        orc->attacker     = new Attacker(3);
        orc->ai           = new MonsterAi();
        engine.actors.push(orc);
    } else {
        // add a troll
        Actor *troll = new Actor(x, y, 'T', "troll", TCODColor::darkerGreen);
        troll->destructible = new MonsterDestructible(16, 1, "troll carcass", 100);
        troll->attacker     = new Attacker(4);
        troll->ai           = new MonsterAi();
        engine.actors.push(troll);
    }
}

void Map::addItem(int x, int y)
{
    TCODRandom *rng = TCODRandom::getInstance();
    int dice = rng->getInt(0, 100);
    static const int HP_POTION_ROLL = 50;
    static const int LIGHTNING_ROLL = HP_POTION_ROLL + 25;
    static const int FIREBALL_ROLL  = LIGHTNING_ROLL + 15;
    static const int CONFUSER_ROLL  = FIREBALL_ROLL + 10;

    if (dice < HP_POTION_ROLL) {
        // get an health potion
        Actor *healthPotion    = new Actor(x, y, '!', "Health Potion", TCODColor::violet);
        healthPotion->blocks   = false;
        healthPotion->pickable = new Healer(4);
        engine.actors.push(healthPotion);
    } else if (dice < LIGHTNING_ROLL) {
        // scroll of lightning bolt
        Actor *scrollOfLightning    = new Actor(x, y, '#', "Scroll of lightning bolt", TCODColor::lightYellow);
        scrollOfLightning->blocks   = false;
        scrollOfLightning->pickable = new LightningBolt(5, 20);
        engine.actors.push(scrollOfLightning);
    } else if (dice < FIREBALL_ROLL) {
        // scroll of lightning bolt
        Actor *scrollOfFireball    = new Actor(x, y, '#', "Scroll of Fireball", TCODColor::lightYellow);
        scrollOfFireball->blocks   = false;
        scrollOfFireball->pickable = new Fireball(6, 12);
        engine.actors.push(scrollOfFireball);
    } else if (dice < CONFUSER_ROLL) {
        // scroll of lightning bolt
        Actor *scrollOfConfusion    = new Actor(x, y, '#', "Scroll of Confusion", TCODColor::lightYellow);
        scrollOfConfusion->blocks   = false;
        scrollOfConfusion->pickable = new Confuser(10, 8);
        engine.actors.push(scrollOfConfusion);
    }
}

/**
 * Dig a 'hole' on the map, from (x1,y1) to (x2,y2) creating a room or corridor
*/
void Map::dig(int x1, int y1, int x2, int y2)
{
    // handle coordinates priority and swap values if needed
    if (x2 < x1) { int tmp = x2; x2 = x1; x1 = tmp; }
    if (y2 < y1) { int tmp = y2; y2 = y1; y1 = tmp; }

    for (int tileX=x1; tileX <= x2; tileX++) {
        for(int tileY=y1; tileY <= y2; tileY++) {
            map->setProperties(tileX, tileY, true, true);
        }
    }
}

/**
 * Create a room and decide if an NPC is to be put in it
 */
void Map::createRoom(bool first, int x1, int y1, int x2, int y2, bool withActors)
{
    dig(x1, y1, x2, y2);
    if (!withActors) return;

    if (first) {
        // player goes in the center of first dug room
        engine.player->x = (x1 + x2) / 2;
        engine.player->y = (y1 + y2) / 2;
    } else {
        TCODRandom *rng=TCODRandom::getInstance();
        int nbMonsters = rng->getInt(0, MAX_ROOM_MONSTERS);
        // try to add the randomized number of monsters in the room
        while (nbMonsters > 0) {
            int x = rng->getInt(x1, x2);
            int y = rng->getInt(y1, y2);
            if (canWalk(x, y)) { addMonster(x, y); }
            nbMonsters--;
        }

        // add items
        int nbItems = rng->getInt(0, MAX_ROOM_ITEMS);
        // try to add the randomized number of monsters in the room
        while (nbItems > 0) {
            int x = rng->getInt(x1, x2);
            int y = rng->getInt(y1, y2);
            if (canWalk(x, y)) { addItem(x, y); }
            nbItems--;
        }
        // set stairs position on the last created room
        engine.stairs->x = (x1 + x2) / 2;
        engine.stairs->y = (y1 + y2) / 2;
    }
}


/**
 * Calcualte the player's field of view on the map, 
 */
void Map::computeFov()
{
    map->computeFov(engine.player->x, engine.player->y, engine.fovRadius);
    // update scent field on tiles
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            if (isInFov(x, y)) {
                unsigned int oldScent = getScent(x, y);
                int dx = x - engine.player->x;
                int dy = y - engine.player->y;
                long distance = (int)sqrt(dx*dx + dy*dy);
                unsigned int newScent = currentScentValue - distance;
                if (newScent > oldScent) {
                    tiles[x + y*width].scent = newScent;
                }
            }
        }
    }
}

void Map::render() const
{
    static const TCODColor darkWall(0,0,100);
    static const TCODColor darkGround(50,50,150);
    static const TCODColor lightWall(130, 110, 50);
    static const TCODColor lightGround(200, 180, 50);

    for (int x=0; x < width; x++) {
        for (int y=0; y < height; y++) {
            if (isInFov(x, y)) {
                TCODConsole::root->setCharBackground(x, y,
                    isWall(x,y) ? lightWall : lightGround);
            } else if (isExplored(x, y)) {
                TCODConsole::root->setCharBackground(x, y,
                    isWall(x,y) ? darkWall : darkGround);
            }
        }
    }
}

void Map::increaseScentValue()
{
    currentScentValue++;
}

unsigned int Map::getScentThreshold() const
{
    return scentThreshold;
}

unsigned int Map::getCurrentScentValue() const
{
    return currentScentValue;
}

unsigned int Map::getScent(int x, int y) const
{
    return tiles[x + y*width].scent;
}