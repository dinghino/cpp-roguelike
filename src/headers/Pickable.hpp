class Pickable: public Persistent {
protected:
    enum PickableType {
        HEALER, LIGHTNING_BOLT, CONFUSER, FIREBALL
    };

public:
    bool pick(Actor *owner, Actor *wearer);
    void drop(Actor *owner, Actor *wearer);
    virtual bool use(Actor *owner, Actor *wearer);

    static Pickable *create(TCODZip &zip);
};

class Healer : public Pickable {
public:
    float amount;       // how many hp can restore
    Healer(float amount);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    bool use(Actor* owner, Actor *wearer);
};


class LightningBolt : public Pickable {
public:
    float range,        // scroll's use range
          damage;       // damage dealth to target creature

    LightningBolt(float range, float damage);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    bool use(Actor *owner, Actor *wearer);
};


class Fireball : public LightningBolt {
public:
    Fireball(float range, float damage);

    void save(TCODZip &zip);

    bool use(Actor *owner, Actor *wearer);
};

class Confuser : public Pickable {
public:
    int nbTurns;    // turns it confuses
    float range;    // use range

    Confuser(int nbTurns, float range);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    bool use(Actor *owner, Actor *wearer);
};