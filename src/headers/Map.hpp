struct Tile {
    bool explored;               // seen by player?
    unsigned int scent;          // amount of player's scent on cell
    Tile(): explored(false), scent(0) {}
};

class Map: public Persistent {
private:
    unsigned int currentScentValue;
    unsigned int scentThreshold;

protected:
    Tile *tiles;
    friend class BspListener;
    TCODMap *map;

    long seed;      // used to generate the map, saved and loaded
    TCODRandom *rng;

    void dig(int x1, int y1, int x2, int y2);
    void createRoom(bool first, int x1, int y1, int x2, int y2, bool withActors);

    void addMonster(int x, int y);
    void addItem(int x, int y);

public:
    int width, height;

    Map(int width, int height);
    ~Map();

    void init(bool withActors);
    void save(TCODZip &zip);
    void load(TCODZip &zip);

    bool isWall(int x, int y) const;
    bool canWalk(int x, int y) const;
    /* Field of View */
    bool isInFov(int x, int y) const;
    bool isExplored(int x, int y) const;
    void computeFov();

    /* Scent tracking */
    unsigned int getScentThreshold() const;
    unsigned int getScent(int x, int y) const;
    void increaseScentValue();
    unsigned int getCurrentScentValue() const;

    void render() const;
};