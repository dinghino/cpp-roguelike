class Actor: public Persistent {
public:
    int         x, y; // position on map
    int         ch;   // ascii code for actor's character
    TCODColor   col;  // color

    // Actor's features ----------------------------------------------------- //
    const char *name;           // actor's name
    bool blocks;                // walkable over or blocks the path
    bool fovOnly;               // only displays when in FOV if true

    // Feature class pointers, not NULL if...
    Attacker *attacker;         // deals damage
    Destructible *destructible; // can be damaged
    Ai *ai;                     // self updating
    Pickable *pickable;         // can be picked and used
    Container *container;       // can contain actors

    // ---------------------------------------------------------------------- //
    Actor(int x, int y, int ch, const char *name, const TCODColor &col);
    ~Actor();

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    // NPC update and movement
    void update();
    void render() const;

    // Helpers -------------------------------------------------------------- //
    float getDistance(int cx, int cy) const;
};
