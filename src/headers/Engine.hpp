class Engine {
public:
    enum GameStatus {
        STARTUP,
        IDLE,
        NEW_TURN,
        VICTORY,
        DEFEAT
    } gameStatus;

    TCODList<Actor *> actors;
    Actor   *player;
    Actor   *stairs;
    Map     *map;
    Gui     *gui;

    int     level;
    int fovRadius;
    int screenWidth;
    int screenHeight;
    TCOD_key_t      lastKey;         // last key pressed by player
    TCOD_mouse_t    mouse;

    Engine(int screenWidth, int screenHeight);
    ~Engine();

    // game status ---------------------------------------------------------- //
    void init();
    void term();
    void load(bool pause=false);
    void save();

    // game loop ------------------------------------------------------------ //
    void update();
    void render();

    // helpers -------------------------------------------------------------- //
    void nextLevel();
    void sendToBack(Actor *actor);
    Actor *getClosestMonster(int x, int y, float range) const;
    Actor *getActor(int x, int y) const;
    bool pickATile(int *x, int *y, float maxRange = 0.0f);
};
 
extern Engine engine;