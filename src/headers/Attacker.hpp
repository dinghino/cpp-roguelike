class Attacker: public Persistent {
public:
    float power;    // hp given

    Attacker(float power);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    void attack(Actor *owner, Actor *target);
};