class Destructible: public Persistent {
protected:
    enum DestructibleType { MONSTER, PLAYER };

public:
    float       maxHP;            // max health
    float       hp;               // current hp
    float       defense;          // hp saved on attacks
    const char  *corpseName;      // actor's name once dead

    int         xp;               // XP gained from the killer (or player xp)

    Destructible(float maxHP, float defense, const char *corpseName, int xp);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    static Destructible *create(TCODZip &zip);

    inline bool isDead() { return hp <= 0; }

    float heal(float amount);
    float takeDamage(Actor *owner, float damage);
    virtual void die(Actor *owner);
};

class MonsterDestructible: public Destructible {
public:
    MonsterDestructible(float maxHP, float defense, const char *corpseName, int xp);
    void save(TCODZip &zip);

    void die(Actor *owner);
};

class PlayerDestructible: public Destructible {
public:
    PlayerDestructible(float maxHP, float defense, const char *corpseName);
    void save(TCODZip &zip);

    void die(Actor *owner);
};