class Container: public Persistent {
public:
    int size;                       // max number of actor, 0 = unlimited
    TCODList<Actor *> inventory;

    Container(int size);
    ~Container();

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    bool add(Actor *actor);
    void remove(Actor *actor);
};