class Ai: public Persistent {
public:
    virtual void update(Actor *owner) = 0;
    static Ai *create(TCODZip &zip);

protected:
    enum AiType {
        MONSTER, CONFUSED_MONSTER, PLAYER
    };
};

class MonsterAi : public Ai {
public:
    MonsterAi();

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    void update(Actor *owner);

protected:
    void moveOrAttack(Actor *owner, int targetX, int targetY);
};

class PlayerAi : public Ai {
public:

    int xpLevel;
    PlayerAi();
    int getNextLevelXp();

    void update(Actor *owner);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

protected:
    bool moveOrAttack(Actor *owner, int targetX, int targetY);
    void handleActionkey(Actor *owner, int ascii);
    Actor *choseFromInventory(Actor *owner);
};

class ConfusedMonsterAi : public Ai {
public:
    ConfusedMonsterAi(int nbTurns, Ai *oldAi);

    void load(TCODZip &zip);
    void save(TCODZip &zip);

    void update(Actor *owner);

protected:
    int nbTurns;
    Ai *oldAi;
};
