#include <stdio.h>
#include <math.h>
#include "main.hpp"

// how many turns a monster can track the player out of FOV
static const int TRACKING_TURNS = 3;

// Player's XP constants
const int LEVEL_UP_BASE = 200;
const int LEVEL_UP_FACTOR = 150;

//
// Player's AI
//

PlayerAi::PlayerAi() : xpLevel(1)
{
}

int PlayerAi::getNextLevelXp()
{
    return LEVEL_UP_BASE + xpLevel * LEVEL_UP_FACTOR;
}

void PlayerAi::update(Actor *owner)
{
    // cannot update if dead!
    if (owner->destructible && owner->destructible->isDead()) return;

    // handle levelling up
    int levelUpXp = getNextLevelXp();
    if (owner->destructible->xp >= levelUpXp) {
        xpLevel++;
        owner->destructible->xp -= levelUpXp;
        // update the menu screen with level up options and show it
        engine.gui->message(TCODColor::azure, "You're getting stronger!\nYou reached level %d", xpLevel);
        engine.gui->menu.clear();
        engine.gui->menu.addItem(Menu::CONSTITUTION, "Constitution ( +20HP )");
        engine.gui->menu.addItem(Menu::STRENGTH, "Strength ( +1 attack )");
        engine.gui->menu.addItem(Menu::AGILITY, "Agility ( +1 defense )");
        // get the desired level up bonus and assign it
        Menu::MenuItemCode menuItem = engine.gui->menu.pick(Menu::PAUSE);
        switch(menuItem) {
            case Menu::CONSTITUTION:
                owner->destructible->maxHP += 20;
                owner->destructible->hp += 20;
                break;
            case Menu::STRENGTH:
                owner->attacker->power += 1;
                break;
            case Menu::AGILITY:
                owner->destructible->defense += 1;
                break;
            default: break;
        }
    }

    // handle movements
    int dx = 0, dy = 0;
    switch (engine.lastKey.vk) {
        case TCODK_UP:      dy = -1; break;
        case TCODK_DOWN:    dy = 1; break;
        case TCODK_LEFT:    dx = -1; break;
        case TCODK_RIGHT:   dx = 1; break;
        case TCODK_CHAR:    handleActionkey(owner, engine.lastKey.c); break;
        default: break;
    }

    if (dx != 0 || dy != 0) {
        engine.gameStatus = Engine::NEW_TURN;
        if (moveOrAttack(owner, owner->x + dx, owner->y + dy)) {
            engine.map->computeFov();
        }
    }
}

bool PlayerAi::moveOrAttack(Actor *owner, int targetX, int targetY)
{
    if (engine.map->isWall(targetX, targetY)) return false;

    // look for living actors to attack
    for (Actor **pActor = engine.actors.begin(); pActor != engine.actors.end(); pActor++) {
        Actor *actor = *pActor;
        bool presence = actor->destructible && !actor->destructible->isDead();

        if (presence && actor->x == targetX && actor->y == targetY) {
            owner->attacker->attack(owner, actor);
            return false;
        }
    }

    // nobody to kill. look for corpses
    for (Actor **pActor = engine.actors.begin(); pActor != engine.actors.end(); pActor++) {
        Actor *actor = *pActor;
        bool corpseOrItem = (actor->destructible && actor->destructible->isDead()) || actor->pickable;

        if (corpseOrItem && actor->x == targetX && actor->y == targetY) {
            engine.gui->message(TCODColor::lightGrey, "There's a %s here\n", actor->name);
        }
    }
    owner->x = targetX;
    owner->y = targetY;
}

void PlayerAi::handleActionkey(Actor *owner, int ascii)
{
    switch (ascii) {
        case 'g':   // pick up item
        {
            bool found=false;
            for (Actor **it = engine.actors.begin(); it != engine.actors.end(); it++) {
                Actor *actor = *it;
                if (actor->pickable && actor->x == owner->x && actor->y == owner->y) {
                    if (actor->pickable->pick(actor, owner)) {
                        found = true;
                        engine.gui->message(TCODColor::lightGreen, "You pick up the %s", actor->name);
                        break;
                    } else if (!found) {
                        found=true;
                        engine.gui->message(TCODColor::red, "Your inventory is full.");
                    }
                }
            }
            if (!found) engine.gui->message(TCODColor::lightGrey, "There's nothing to pick up.");
            engine.gameStatus = Engine::NEW_TURN;
        }
        break;
        case 'i':
        {
            Actor *actor = choseFromInventory(owner);
            if (actor) {
                if(actor->pickable->use(actor, owner))
                engine.gui->message(TCODColor::darkYellow, "You used a %s", actor->name);

                engine.gameStatus=Engine::NEW_TURN;
            }
        }
        break;
        case 'd':
        {
            Actor *actor = choseFromInventory(owner);
            if (actor) {
                actor->pickable->drop(actor, owner);
                engine.gameStatus = Engine::NEW_TURN;
            }
        }
        break;
        case '>':
        {
            if (engine.stairs->x == owner->x && engine.stairs->y == owner->y) {
                engine.nextLevel();
            } else {
                engine.gui->message(TCODColor::lightGrey, "There are no stairs here.");
            }
        }
        break;
        default: break;
    }
}

Actor *PlayerAi::choseFromInventory(Actor *owner)
{
    static const int INVENTORY_WIDTH = 50;
    static const int INVENTORY_HEIGHT = 28;
    static TCODConsole con(INVENTORY_WIDTH, INVENTORY_HEIGHT);

    // inventory frame
    con.setDefaultForeground(TCODColor(200, 180, 50));
    con.printFrame(0, 0, INVENTORY_WIDTH, INVENTORY_HEIGHT, true,
                    TCOD_BKGND_DEFAULT, "Inventory");

    // display player's items with keyboard shortcut
    con.setDefaultForeground(TCODColor::white);
    int shortcut = 'a';
    int y = 1;
    for (Actor **it = owner->container->inventory.begin();
            it != owner->container->inventory.end(); it++) {
        Actor *actor = *it;
        con.print(2, y, "( %c ) - %s", shortcut, actor->name);
        y++; shortcut++;
    }

    // blit inventory on root console
    TCODConsole::blit(&con, 0, 0, INVENTORY_WIDTH, INVENTORY_HEIGHT, TCODConsole::root,
        engine.screenWidth/2 - INVENTORY_WIDTH/2,
        engine.screenHeight/2 - INVENTORY_HEIGHT/2);
    TCODConsole::flush();

    // wait for user's input
    TCOD_key_t key;
    TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL, true);
    if (key.vk == TCODK_CHAR) {
        int actorIndex = key.c - 'a';
        // if the input is a character in range of the inventory size get that
        // item and return it, else return NULL (exit inventory)
        if (actorIndex >= 0 && actorIndex < owner->container->inventory.size()) {
            return owner->container->inventory.get(actorIndex);
        }
        return NULL;
    }
}

//
// Monster's AI logic
//

MonsterAi::MonsterAi() {}

void MonsterAi::update(Actor *owner)
{
    if (owner->destructible && owner->destructible->isDead()) return;
    moveOrAttack(owner, engine.player->x, engine.player->y);
}

void MonsterAi::moveOrAttack(Actor *owner, int targetX, int targetY)
{
    int dx = targetX - owner->x;
    int dy = targetY - owner->y;

    float distance = sqrtf(dx*dx + dy*dy);

    if (distance < 2) {
        // target in melee range. attack if possible.
        if (owner->attacker) owner->attacker->attack(owner, engine.player);
        return;
    } else if (engine.map->isInFov(owner->x, owner->y)) {
        // target is in sight. move.
        dx = (int)(round(dx / distance));
        dy = (int)(round(dy / distance));
        if (engine.map->canWalk(owner->x + dx, owner->y + dy)) {
            owner->x += dx;
            owner->y += dy;
            return;
        }
    }

    // player not in FOV of the monster. try scent tracking
    unsigned int bestLevel = 0;
    int bestCellIdx = -1; // -1 means no luck :(
    // nearby cells delta-coords
    static int tdx[8]{ -1, 0, 1, -1, 1, -1, 0, 1 };
    static int tdy[8]{ -1, -1, -1, 0, 0, 1, 1, 1 };
    // loop all nearby cells and smell for target's scent
    for (int i = 0; i < 8; i++) {
        int cellX = owner->x + tdx[i];
        int cellY = owner->y + tdy[i];
        if (engine.map->canWalk(cellX, cellY)) {
            unsigned int cellScent = engine.map->getScent(cellX, cellY);
            if (cellScent > engine.map->getCurrentScentValue() - engine.map->getScentThreshold()
                    && cellScent > bestLevel) {
                // found a "best smelling cell". Set it as next destination
                bestLevel = cellScent;
                bestCellIdx = i;
            }
        }
    }
    if (bestCellIdx != -1) {
        // scent found! move there and follow.
        owner->x += tdx[bestCellIdx];
        owner->y += tdy[bestCellIdx];
    }
}

ConfusedMonsterAi::ConfusedMonsterAi(int nbTurns, Ai *oldAi) : nbTurns(nbTurns), oldAi(oldAi) {}

void ConfusedMonsterAi::update(Actor *owner)
{
    TCODRandom *rng = TCODRandom::getInstance();
    int dx = rng->getInt(-1, 1);
    int dy = rng->getInt(-1, 1);

    if (dx != 0 || dy != 0) {
        int destx = owner->x + dx;
        int desty = owner->y + dy;
        if(engine.map->canWalk(destx, desty)) {
            owner->x = destx;
            owner->y = desty;
        // if the owner cannot walk there, check if we have an actor and attack
        // it regardless of faction
        } else {
            Actor *actor = engine.getActor(destx, desty);
            if(actor && actor->attacker) owner->attacker->attack(owner, actor);
        }
    }
    nbTurns--;
    // when the confusion turns end restore the previous AI
    if(nbTurns == 0) {
        owner->ai = oldAi;
        delete this;
    }
}
