#include <iostream>
#include "main.hpp"

Engine::Engine(int screenWidth, int screenHeight) : gameStatus(STARTUP),
fovRadius(10), screenWidth(screenWidth), screenHeight(screenHeight), level(1)
{
    TCODConsole::initRoot(80,50, "RogueLike tutorial", false);
    gui = new Gui();
}

void Engine::init()
{
    player               = new Actor(40, 25, '@', "player", TCODColor::white);
    player->destructible = new PlayerDestructible(30, 2, "Your cadaver");
    player->attacker     = new Attacker(5);
    player->ai           = new PlayerAi();
    player->container    = new Container(26);
    actors.push(player);

    stairs          = new Actor(0, 0, '>', "stairs", TCODColor::white);
    stairs->blocks  = false;
    stairs->fovOnly = false;
    actors.push(stairs);

    map = new Map(80, 43);
    map->init(true);

    gui->message(TCODColor::red,
        "Welcome Stranger!\nPrepare to perish in Trambusti' smelly lair.");
    gameStatus = STARTUP;
}

Engine::~Engine()
{
    actors.clearAndDelete();
    term();
    delete gui;
}

void Engine::term()
{
    actors.clearAndDelete();
    if (map) delete map;
    gui->clear();
}

void Engine::update()
{
    if (gameStatus == STARTUP) map->computeFov();
    gameStatus = IDLE;
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
    switch (lastKey.vk) {
        case TCODK_ESCAPE:
            std::cout << "ESC key pressed" << std::endl;
            save();
            load(true);
            break;
        case TCODK_F11:
            std::cout << "Toggling full screen" << std::endl;
            TCODConsole::setFullscreen(!TCODConsole::isFullscreen());
            break;
        default: break;
    };

    player->update();
    if (gameStatus == NEW_TURN) {
        map->increaseScentValue();
        for (Actor **pActor=actors.begin(); pActor != actors.end(); pActor++) {
            Actor *actor = *pActor;
            if (actor != player) actor->update();
        }
    }
}

void Engine::render()
{
    TCODConsole::root->clear();
    map->render();

    /*
    The TCODList begin() function returns a pointer to the first element.
    Since our elements are already Actor pointers, we get a pointer
    to a pointer to an Actor : Actor **.
    We use the indirection operator (*iterator) to retrieve the object behind
    the pointer (the actual Actor pointer) and the dereference operator -> to
    access a member on the pointed object (the actual Actor).
    */
    for (Actor **pActor=actors.begin(); pActor != actors.end(); pActor++) {
        Actor *actor = *pActor; // get the Actor from the pointer

        // draw actor if is not the player (rendered after) AND either
        // * is NOT fovOnly (stairs) AND the map is explored -> shows ALWAYS after it's found
        // * is in the player's field of view
        if (actor != player
            && ((!actor->fovOnly && map->isExplored(actor->x, actor->y))
            || map->isInFov(actor->x, actor->y))) {
                    actor->render();
            }
    }
    player->render();
    gui->render();
}

// -------------------------------------------------------------------------- //
// Helpers
// -------------------------------------------------------------------------- //

void Engine::nextLevel()
{
    level++;

    gui->message(TCODColor::lightViolet, "You take a moment to rest and recover your strenght.");
    // regain half of total HP
    player->destructible->heal(player->destructible->maxHP / 2);
    gui->message(TCODColor::red, "After a rare moment of peace\nyou decend deeper into\nthe smelly dungeon...");

    // delete the current level's map and all the actors but player and stairs
    delete map;
    for (Actor **it = actors.begin(); it != actors.end(); it++) {
        if (*it != player && *it != stairs) {
            delete *it;
            it = actors.remove(it); // TCODList.remove allows removing while iterating
        }
    }

    // ... and create the new level
    map = new Map(80, 43);
    map->init(true);
    gameStatus = STARTUP;
}

void Engine::sendToBack(Actor *actor)
{
    actors.remove(actor);
    actors.insertBefore(actor, 0);
}

Actor *Engine::getClosestMonster(int x, int y, float range) const
{
    Actor *closest=NULL;
    float bestDistance = 1E6f;      // infinity as far as we care

    for (Actor **it = actors.begin(); it != actors.end(); it++) {
        Actor *actor = *it;
        // loop all alive enemies
        if (actor != player && actor->destructible && !actor->destructible->isDead()) {
            float distance = actor->getDistance(x, y);
            if (distance < bestDistance && (distance <= range || range == 0.0f)) {
                bestDistance = distance;
                closest = actor;
            }
        }
    }
    return closest;
}

Actor *Engine::getActor(int x, int y) const
{
    for (Actor **it = actors.begin(); it != actors.end(); it++) {
        Actor *actor = *it;
        if (actor->x == x && actor->y == y && actor->destructible && !actor->destructible->isDead()) {
            return actor;
        }
    }
    return NULL;
}

bool Engine::pickATile(int *x, int *y, float maxRange)
{
    while (!TCODConsole::isWindowClosed()) {
        render();
        // higlight possible range
        for (int cx = 0; cx < map->width; cx++) {
            for (int cy = 0; cy < map->height; cy++) {
                if (map->isInFov(cx, cy) && (maxRange == 0 || player->getDistance(cx, cy) <= maxRange)) {
                        TCODColor col = TCODConsole::root->getCharBackground(cx, cy);
                    col = col * 1.2f;
                    TCODConsole::root->setCharBackground(cx, cy, col);
                }
            }
        }

        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);

        if (map->isInFov(mouse.cx, mouse.cy) && (maxRange == 0 || player->getDistance(mouse.cx, mouse.cy) <= maxRange)) {
            TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::white);
            if (mouse.lbutton_pressed) {
                *x = mouse.cx;
                *y = mouse.cy;
                return true;
            }
        }
        if (mouse.rbutton_pressed || lastKey.vk != TCODK_NONE) {
            return false;
        }
        TCODConsole::flush();
    }
    return false;
}