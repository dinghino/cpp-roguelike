#include <stdio.h>
#include <stdarg.h>
#include "main.hpp"


static const int PANEL_HEIGHT   = 7;
static const int BAR_WIDTH      = 20;
static const int MSG_X          = BAR_WIDTH + 2;
static const int MSG_HEIGHT     = PANEL_HEIGHT - 1;

Gui::Gui()
{
    con = new TCODConsole(engine.screenWidth, PANEL_HEIGHT);
}
Gui::~Gui()
{
    delete con;
    clear();
}

void Gui::clear()
{
    log.clearAndDelete();
}

// duplicate the text var on creation and free it on deletion using C functions
Gui::Message::Message(const char *text, const TCODColor &col): text(strdup(text)), col(col) {}
Gui::Message::~Message() { free(text); }

void Gui::message(const TCODColor &col, const char *text, ...)
{
    // build text
    va_list ap;
    char buf[128];
    va_start(ap, text);
    vsprintf(buf, text, ap);
    va_end(ap);

    // do simil line wrapping
    char *lineBegin=buf;
    char *lineEnd;

    do {
        // make room for new message
        if (log.size() == MSG_HEIGHT) {
            Message *toRemove = log.get(0);
            log.remove(toRemove);
            delete toRemove;
        }
        lineEnd = strchr(lineBegin, '\n');
        if (lineEnd) {*lineEnd = '\0'; }

        // we split the message, add to the log while it has parts
        Message *msg = new Message(lineBegin, col);
        log.push(msg);
        lineBegin = lineEnd + 1;
    } while(lineEnd);
}

void Gui::render()
{
    // Clear console
    con->setDefaultBackground(TCODColor::black);
    con->clear();
    // draw bar
    renderBar(1, 2, BAR_WIDTH, "HP",
        engine.player->destructible->hp, engine.player->destructible->maxHP,
        TCODColor::lightRed, TCODColor::darkerRed
    );

    // draw message log
    int y=1;
    float colorCoef = 0.4f;    // color coefficient to darken older messages
    for (Message **it = log.begin(); it != log.end(); it++) {
        Message *message = *it;
        con->setDefaultForeground(message->col * colorCoef);
        con->print(MSG_X,y, message->text);
        y++;
        if (colorCoef < 1.0f) { colorCoef += 0.3f; }
    }
    renderMouseLook();

    // show the current dungeon level
    con->setDefaultForeground(TCODColor::white);
    con->print(3, 3, "Dungeon level: %d", engine.level);

    // draw the player's xp bar
    PlayerAi *ai = (PlayerAi *)engine.player->ai;
    char xpTxt[128];
    sprintf(xpTxt, "XP(%d)", ai->xpLevel);
    renderBar(1, 5, BAR_WIDTH, xpTxt,
        engine.player->destructible->xp, ai->getNextLevelXp(),
        TCODColor::lightViolet, TCODColor::darkerViolet);

    // blit GUI on root console
    TCODConsole::blit(con, 0, 0, engine.screenWidth, PANEL_HEIGHT,
        TCODConsole::root, 0, engine.screenHeight - PANEL_HEIGHT);
}

void Gui::renderBar(int x, int y, int width, const char *name,
    float value, float maxValue, const TCODColor &barColor,
    const TCODColor &backColor)
{
        // fill the background
        con->setDefaultBackground(backColor);
        con->rect(x,y,width,1,false,TCOD_BKGND_SET);

        // compute the width of the health section
        int barWidth = (int)(value / maxValue * width);
        if (barWidth > 0) {
            con->setDefaultBackground(barColor);
            con->rect(x, y, barWidth, 1, false, TCOD_BKGND_SET);
        }
        // print values
        con->setDefaultForeground(TCODColor::white);
        con->printEx(x+ width/2, y, TCOD_BKGND_NONE, TCOD_CENTER,
        "%s : %g/%g", name, value, maxValue);
}

void Gui::renderMouseLook()
{
    // nothing to render if mouse not in fov
    if (!engine.map->isInFov(engine.mouse.cx, engine.mouse.cy)) return;
    
    // write a comma-separated list of what's in the cell
    char buf[128] = "";
    bool first = true;
    for (Actor **it = engine.actors.begin(); it != engine.actors.end(); it++) {
        Actor *actor = *it;
        if(actor->x == engine.mouse.cx && actor->y == engine.mouse.cy) {
            if (!first) strcat(buf, ", ");
            else first = false;
            
            strcat(buf, actor->name);
        }
    }
    con->setDefaultForeground(TCODColor::lightGrey);
    con->print(1,0,buf);
}

// -------------------------------------------------------------------------- //
// Menu
// -------------------------------------------------------------------------- //

Menu::~Menu()
{
    clear();
}

void Menu::clear()
{
    items.clearAndDelete();
}

void Menu::addItem(MenuItemCode code, const char *label)
{
    MenuItem *item = new MenuItem();
    item->code  = code;
    item->label = label;
    items.push(item);
}

const int PAUSE_MENU_WIDTH  = 30;
const int PAUSE_MENU_HEIGHT = 15;
Menu::MenuItemCode Menu::pick(Menu::DisplayMode mode)
{
    int selectedItem = 0;
    int menuX, menuY;
    
    if (mode == PAUSE) {
        menuX = engine.screenWidth / 2 - PAUSE_MENU_WIDTH / 2;
        menuY = engine.screenHeight / 2 - PAUSE_MENU_HEIGHT / 2;

        TCODConsole::root->setDefaultForeground(TCODColor(200, 180, 50));
        TCODConsole::root->printFrame(menuX, menuY, PAUSE_MENU_WIDTH, PAUSE_MENU_HEIGHT,
            true, TCOD_BKGND_ALPHA(70), "menu");

        menuX += 2;
        menuY += 3;
    } else {
        static TCODImage img("menu_background1.png");
        img.blit2x(TCODConsole::root, 0, 0);
        menuX = 10;
        menuY = TCODConsole::root->getHeight() / 3;
    }

    while (!TCODConsole::isWindowClosed()) {
        int currentItem = 0;
        // create the actual menu from the menu items available and highlight
        // the one selected
        for (MenuItem **it = items.begin(); it != items.end(); it++) {
            if (currentItem == selectedItem) {
                TCODConsole::root->setDefaultForeground(TCODColor::lighterOrange);
            } else {
                TCODConsole::root->setDefaultForeground(TCODColor::lightGrey);
            }
            TCODConsole::root->print(menuX, menuY + currentItem * 3, (*it)->label);
            currentItem++;
        }
        TCODConsole::flush();

        // check keypress
        TCOD_key_t key;
        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL);
        switch(key.vk) {
            case TCODK_UP:
                selectedItem--;
                if (selectedItem < 0) selectedItem = items.size() - 1;
                break;
            case TCODK_DOWN:
                selectedItem = (selectedItem + 1) % items.size();
                break;
            case TCODK_ENTER:
                return items.get(selectedItem)->code;
                break;
            default: break;
        }
    }
    return NONE;    // from the enum
}
